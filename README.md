# NxMicrofrontend

https://zerotacg.gitlab.io/learning-nx-microfrontend/

## steps to recreate

```
npx create-nx-workspace@latest nx-microfrontend --preset=ts
cd nx-microfrontend
npm install --save-dev @nx/react
npx nx generate @nx/react:host app-shell --compiler=swc --remotes=micro-dashboard,micro-greeting
```
