import { Button } from "@mui/material";
import * as React from "react";
import { FC, ReactNode } from "react";
import { Link as RouterLink, LinkProps as RouterLinkProps } from "react-router-dom";

export interface Props {
  children?: ReactNode;
  to: RouterLinkProps["to"];
}

export const AppBarNavButton: FC<Props> = (props) => (
  <Button
    component={RouterLink}
    sx={{ my: 2, color: "white", display: "block" }}
    {...props}
  />
);
