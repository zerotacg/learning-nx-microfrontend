import { Button, Container, List, ListItem, ListItemText, styled } from "@mui/material";

const StyledRoot = styled("div")(({ theme }) => ({
  margin: theme.spacing(2)
}));

const StyledHeader = styled("h1")(({ theme }) => ({
  ...theme.typography.h3,
  marginBottom: theme.spacing(2)
}));
const StyledListItem = styled(ListItem)(({ theme }) => ({
  padding: 0,
  marginBottom: theme.spacing(1)
}));

export function App() {
  const greetings = ["Hello", "Good morning", "Good afternoon", "Good evening"];

  return (
    <Container maxWidth="xl">
      <Button variant="contained" color="primary">
        Press me primary
      </Button>
      <Button variant="contained" color="secondary">
        Press me secondary
      </Button>
      <StyledRoot>
        <StyledHeader>Greetings</StyledHeader>
        <List>
          {greetings.map((greeting) => (
            <StyledListItem key={greeting}>
              <ListItemText primary={`${greeting}!`} />
            </StyledListItem>
          ))}
        </List>
      </StyledRoot>
    </Container>
  );
}

export default App;
