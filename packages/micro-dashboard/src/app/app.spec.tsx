import { render } from '@testing-library/react';

import App from './app';

describe('App', () => {
  it('should have a title', () => {
    const { getByText } = render(<App />);
    expect(getByText(/Dashboard/gi)).toBeTruthy();
  });
});
