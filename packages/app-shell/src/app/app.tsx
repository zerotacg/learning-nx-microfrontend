import { createTheme, CssBaseline, ThemeProvider } from "@mui/material";
import { lazy, Suspense } from "react";
import { Route, Routes } from "react-router-dom";
import { AppBar } from "./app-bar/app-bar";
import { AppBarNav } from "./app-bar/app-bar-nav";
import { AppBarNavButton } from "./app-bar/app-bar-nav-button";

const Dashboard = lazy(() => import("micro-dashboard/Module"));
const Greeting = lazy(() => import("micro-greeting/Module"));

const theme = createTheme({
  palette: {
    primary: {
      main: "#f44336"
    },
    secondary: {
      main: "#4caf50"
    }
  }
});

export function App() {
  return (
    <Suspense fallback={null}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <AppBar>
          <AppBarNav>
            <AppBarNavButton to="/">Dashboard</AppBarNavButton>
            <AppBarNavButton to="/greeting">Greeting</AppBarNavButton>
          </AppBarNav>
        </AppBar>
        <Routes>
          <Route path="/" element={<Dashboard />} />

          <Route path="/greeting" element={<Greeting />} />
        </Routes>
      </ThemeProvider>
    </Suspense>
  );
}

export default App;
