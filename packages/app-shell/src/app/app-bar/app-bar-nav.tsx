import { Box, styled } from "@mui/material";
import * as React from "react";

export const AppBarNav = styled(Box)({ flexGrow: 1, display: "flex" });
