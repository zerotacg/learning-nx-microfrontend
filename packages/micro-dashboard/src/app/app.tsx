import { Button, Container, Grid, Paper, styled, Typography } from "@mui/material";
import placeholder from "../assets/600x400.png";

const StyledRoot = styled("div")(({ theme }) => ({
  flexGrow: 1,
  padding: theme.spacing(2)
}));

const StyledPaper = styled(Paper)(({ theme }) => ({
  padding: theme.spacing(2),
  textAlign: "center",
  color: theme.palette.text.secondary
}));

export function App() {
  return (
    <Container maxWidth="xl">
      <StyledRoot>
        <img src={placeholder} alt="placeholder image" />
        <Button variant="contained" color="primary">
          Press me primary
        </Button>
        <Button variant="contained" color="secondary">
          Press me secondary
        </Button>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography variant="h3" component="h1" gutterBottom>
              Dashboard
            </Typography>
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <StyledPaper>
              <Typography variant="h4" component="h2" gutterBottom>
                Users
              </Typography>
              <Typography variant="h5" component="h2">
                1000
              </Typography>
            </StyledPaper>
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <StyledPaper>
              <Typography variant="h4" component="h2" gutterBottom>
                Sales
              </Typography>
              <Typography variant="h5" component="h2">
                $5000
              </Typography>
            </StyledPaper>
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <StyledPaper>
              <Typography variant="h4" component="h2" gutterBottom>
                Orders
              </Typography>
              <Typography variant="h5" component="h2">
                50
              </Typography>
            </StyledPaper>
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <StyledPaper>
              <Typography variant="h4" component="h2" gutterBottom>
                Products
              </Typography>
              <Typography variant="h5" component="h2">
                500
              </Typography>
            </StyledPaper>
          </Grid>
        </Grid>
      </StyledRoot>
    </Container>
  );
}

export default App;
