import AdbIcon from "@mui/icons-material/Adb";
import { AppBar as MuiAppBar, Container, Toolbar, Typography } from "@mui/material";
import React, { FC, PropsWithChildren } from "react";

export const AppBar: FC<PropsWithChildren> = ({ children }) => {
  return (
    <MuiAppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <AdbIcon sx={{ display: "flex", mr: 1 }} />
          <Typography
            variant="h6"
            noWrap
            component="a"
            href="/learning-nx-microfrontend/"
            sx={{
              mr: 2,
              display: "flex",
              fontFamily: "monospace",
              fontWeight: 700,
              letterSpacing: ".3rem",
              color: "inherit",
              textDecoration: "none"
            }}
          >
            LOGO
          </Typography>

          {children}
        </Toolbar>
      </Container>
    </MuiAppBar>
  );
};
